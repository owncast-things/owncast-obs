# Owncast status dock for OBS

An HTML+JS file to be used as a custom dock in OBS.

![Screenshot of OBS with the Owncast status dock](screenshot.png)

## Features

- Shows if your stream is offline or online, and for how long you've been streaming.
- Displays the current title of your stream, and **lets you change it from within obs** by clicking on it. (This is why the stream key is needed).
- Shows how many people are watching your stream.

## How to install:

1. Download `owncast-status.html` and save it somewhere on your computer.
2. Edit the file using your favorite text editor and update the `config` constant with your server's URL and stream key (see example below).
    
    **Warning:** Use this file locally only (do not pu it online on a webserver), or you will leak your stream key!!
    ```js
    const config = {
        owncast_base_url: "https://your-owncast-instance.tld",
        owncast_stream_key: "your_stream_key"
    }
    ```

3. Add a custom dock to OBS :
   1. Click the *Docks* menu, then *Custom Browser Docks…*;
   2. In the "Dock name" column, give the dock a name (e.g. "Stream status");
   3. In the "URL" column, type in the URL to the *owncast-status.html* file. (e.g. `file:///home/john/src/owncast-status.html` or `file:///c:/users/John/mystuff/owncast-status.html`)
      
      [Learn more about file URI schemes on Wikipedia](https://en.wikipedia.org/wiki/File_URI_scheme)

## Change stream title from within OBS

![Screenshot showing the input field for the new title](screenshot-changetitle.png)

1. Click the current title in the custom dock;
2. Write the new title in the text box which appears;
3. Hit the *Enter* key.